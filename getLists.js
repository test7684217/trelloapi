const { token, APIKey } = require("./api");

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to get lists for board '${boardId}': ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.error("Error getting lists:", error);
        reject(error);
      });
  });
}

module.exports = getLists;
