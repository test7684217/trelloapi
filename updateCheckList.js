const { token, APIKey } = require("./api");

function fetchCheckListItems(checkListId) {
  const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${APIKey}&token=${token}`;
  return fetch(url)
    .then((response) => response.json())
    .catch((err) => {
      console.error("Error fetching check items:", err);
      throw err;
    });
}

function updateCheckListItem(cardId, checkItemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${APIKey}&token=${token}`;
    return fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ state: "complete" }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to update check item ${checkItemId}: ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        console.log(`Check item ${checkItemId} updated successfully.`);
        return data;
      })
      .catch((err) => {
        console.error(`Error updating check item ${checkItemId}:`, err);
        throw err;
      });
  }
  

function updateAllCheckListItems(checkListId, cardId) {
  return fetchCheckListItems(checkListId)
    .then((checkItems) => {
      const updatePromises = checkItems.map((item) =>
        updateCheckListItem(cardId, item.id)
      );
      return Promise.all(updatePromises);
    })
    .then((results) => {
      console.log("All check items updated to complete.");
      return results;
    })
    .catch((err) => {
      console.error("Error updating check items:", err);
    });
}


const checkListId = "665026f48ad57cbdf96c89f1";
const cardId = "665026e50d9f08662083d83d";

updateAllCheckListItems(checkListId, cardId).then((data) => console.log(data));
