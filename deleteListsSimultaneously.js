const createBoard = require("./createBoard");
const createCard = require("./createCard");
const createList = require("./createList");
const archiveList = require("./archiveList");

function create1Board3Lists1Card() {
  return new Promise((resolve, reject) => {
    createBoard("Test")
      .then((board) => {
        const listNames = ["List1", "List2", "List3"];
        const listAndCardPromises = listNames.map((listName) => {
          return createList(listName, board.id).then((list) => {
            return createCard(list.id).then((card) => ({
              list,
              card,
            }));
          });
        });
        return Promise.all(listAndCardPromises);
      })
      .then((results) => {
        console.log("Task completed");
        resolve(results);
      })
      .catch((error) => {
        console.error("An error occurred:", error);
        reject(error);
      });
  });
}

create1Board3Lists1Card()
  .then((data) => {
    let listIDs = data.map((item) => item.list.id);
    return deleteAllLists(listIDs);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));

function deleteAllLists(listIDs) {
  return new Promise((resolve, reject) => {
    let archiveListPromises = [];
    for (const listID of listIDs) {
      let archiveListPromise = archiveList(listID);
      archiveListPromises.push(archiveListPromise);
    }
    return Promise.all(archiveListPromises)
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.log(error);
      });
  });
}
