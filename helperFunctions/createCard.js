function createCard(listId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to create card in list ${listId}: ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.error(`Error creating card in list ${listId}:`, error);
        reject(error);
      });
  });
}

module.exports = createCard;
