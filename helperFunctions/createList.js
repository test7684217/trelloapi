function createList(listName, boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to create list '${listName}' on board ${boardId}: ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.error(`Error creating list '${listName}' on board ${boardId}:`, error);
        reject(error);
      });
  });
}

module.exports = createList;
