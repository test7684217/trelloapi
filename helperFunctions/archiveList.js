function archiveList(listId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${APIKey}&token=${token}`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to archive list ${listId}: ${response.statusText}`);
        }
        return response.text();
      })
      .then((text) => resolve(text))
      .catch((err) => {
        console.error(`Error archiving list ${listId}:`, err);
        reject(err);
      });
  });
}

module.exports = archiveList;
