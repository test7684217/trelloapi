function getCheckListID(id) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${id}/checklists?key=${APIKey}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to fetch checklists for card ${id}: ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => resolve(data))
      .catch((error) => {
        console.error(`Error fetching checklists for card ${id}:`, error);
        reject(error);
      });
  });
}

let cardID = "665026e50d9f08662083d83d";

getCheckListID(cardID)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error("Error:", error);
  });
