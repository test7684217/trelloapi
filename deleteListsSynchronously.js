const createBoard = require("./createBoard");
const createCard = require("./createCard");
const createList = require("./createList");
const archiveList = require("./archiveList");

create1Board3Lists1Card()
  .then((data) => {
    let listIDs = data.map((item) => item.list.id);
    return deleteAllLists(listIDs);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));

function create1Board3Lists1Card() {
  return new Promise((resolve, reject) => {
    createBoard("Tester")
      .then((board) => {
        const listNames = ["List1", "List2", "List3"];
        const listAndCardPromises = listNames.map((listName) => {
          return createList(listName, board.id).then((list) => {
            return createCard(list.id).then((card) => ({
              list,
              card,
            }));
          });
        });
        return Promise.all(listAndCardPromises);
      })
      .then((results) => {
        console.log("Task completed");
        resolve(results);
      })
      .catch((error) => {
        console.error("An error occurred:", error);
        reject(error);
      });
  });
}

function deleteAllLists(listIDs) {
  try {
    return listIDs.reduce((acc, currListId) => {
      return acc.then(() => {
        return archiveList(currListId);
      });
    }, Promise.resolve());

  } catch (error) {
    console.log(error);
  }
}
