const createBoard = require("./createBoard");
const createCard = require("./createCard");
const createList = require("./createList");

function create1Board3Lists1Card() {
  return new Promise((resolve, reject) => {
    createBoard("Test")
      .then((board) => {
        const listNames = ["List1", "List2", "List3"];
        const listAndCardPromises = listNames.map((listName) => {
          return createList(listName, board.id)
            .then((list) => createCard(list.id).then((card) => ({ list, card })))
            .catch((error) => {
              throw new Error(`Error creating list '${listName}': ${error.message}`);
            });
        });
        return Promise.all(listAndCardPromises);
      })
      .then((results) => {
        console.log("Task completed");
        resolve(results);
      })
      .catch((error) => {
        console.error("An error occurred:", error);
        reject(error);
      });
  });
}

create1Board3Lists1Card()
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.error("Main error handler:", error);
  });
