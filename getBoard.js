// Create a function getBoard which takes the boardId as argument and returns a promise which resolves with board data

const { APIKey, token, boardId } = require("./api");

function getBoard(boardId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${token}`,
      {
        method: "GET",
        headers: { Accept: "application/json" },
      }
    )
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        resolve(data);
      })
  });
}

getBoard("665026ba921eaa60cf781f08")
  .then((data) => {
    console.log(
      "-----------------Geting board of the board id---------------- ",
      boardId
    );
    console.log(data);
  })
  .catch((err) => console.error(err));

  

module.exports = getBoard;
