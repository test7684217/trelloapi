// Create a function getAllCards which takes a boardId as argument and
// which uses getCards function to fetch cards of all the lists.
// Do note that the cards should be fetched simultaneously from all the lists.

const { boardId } = require("./api");
const getCards = require("./getCards");
const getLists = require("./getLists");

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {

    getLists(boardId)
      .then((lists) => {
        let cardPromises = [];
        for (const list of lists) {
          let cardPromise = getCards(list.id);
          cardPromises.push(cardPromise);
        }
        return Promise.all(cardPromises);
      })
      .then((data) => {
        resolve(data);
      }).catch((error)=>{

      })
  });
}

getAllCards("665026ba921eaa60cf781f08")
.then((allCards) => {
  console.log(
    "-------------- Getting all cards using boardId-----------------",
    boardId
  );
  console.log(allCards)

});
