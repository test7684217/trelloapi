const { token, APIKey } = require("./api");

function createBoard(boardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to create board '${boardName}': ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.error("Error creating board:", error);
        reject(error);
      });
  });
}

const newBoardName = "DemoBoard";

createBoard(newBoardName)
  .then((board) => {
    console.log(board);
  })
  .catch((error) => {
    console.error("Error:", error);
  });

module.exports = createBoard;
