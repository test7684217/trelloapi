const { APIKey, token } = require("./api");

function getCards(listId) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Failed to get cards for list '${listId}': ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        console.error("Error getting cards:", error);
        reject(error);
      });
  });
}

const listId = "664eed782a9332326154fbda";


getCards(listId).then((cards) => {
  console.log(
    "-----------------Getting all the cards of the list id---------------- ",
    listId
  );
  console.log(cards);
});


module.exports = getCards;
